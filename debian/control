Source: libcoro-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>,
           Xavier Guimard <yadd@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libanyevent-perl <!nocheck>,
               libcanary-stability-perl,
               libcommon-sense-perl <!nocheck>,
               libev-perl <!nocheck>,
               libevent-perl <!nocheck>,
               libguard-perl <!nocheck>,
               perl-xs-dev,
               perl:native
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libcoro-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libcoro-perl.git
Homepage: https://metacpan.org/release/Coro
Rules-Requires-Root: no

Package: libcoro-perl
Architecture: any
Depends: ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends},
         libanyevent-perl,
         libcommon-sense-perl,
         libguard-perl
Recommends: libev-perl,
            libevent-perl,
            libio-aio-perl,
            libnet-http-perl
Description: Perl framework implementing coroutines
 Coro is a collection of modules which manages continuations in general, most
 often in the form of cooperative threads (also called coros, or simply "coro"
 in the documentation). They do not actually execute at the same time, even on
 machines with multiple processors.
 .
 The specific flavor of thread offered by this module also guarantees you that
 it will not switch between threads unless necessary. It switches at easily-
 identified points in your program, so locking and parallel access are rarely
 an issue, making threaded programming much safer and easier than using other
 threading models.
 .
 Coro provides a full shared address space, which makes communication between
 threads very easy. A parallel matrix multiplication benchmark runs over 300
 times faster on a single core than perl's ithreads on a quad core using all
 four cores.
